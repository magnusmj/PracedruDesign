#include "idobject.h"

using namespace Model;

IdObject::IdObject(unsigned long id, IDObject::Type type) : id(m_id), m_type(type) {
    m_id = id;
    id_objects[m_id] = this;
}

IdObject::IdObject(IDObject::Type type) : m_type(type), id(m_id) {
    m_id = IdObject::counter++;
    id_objects[m_id] = this;
}

IdObject *IdObject::getIdObject(unsigned long id) {
    if (IdObject::id_objects.find(id) != IdObject::id_objects.end()) {
        return nullptr;
    } else {
        return IdObject::id_objects[id];
    }
}
