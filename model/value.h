#ifndef VALUE_H
#define VALUE_H

#include "observer.h"

namespace Model{
    template <typename T> class Value : public Observable {
        T *m_v;
    public:
        Value(T &v) { m_v = &v; };
        Value &operator=(const T &other){
            setValue(other);
            return *this;
        }
        Value &operator=(const Value<T> &other){
            setValue(*other.m_v);
            return *this;
        }
        operator T() const { return *m_v; }

    private:
        void setValue(const T &v);
    };

    template <typename T>
    struct ValueChange : public ObjectChange {
        T before;
        T after;
        ValueChange(IObservable *sender) : ObjectChange(sender) { type = Change::Value; origin = this; }
    };

    template<typename T>
    void Value<T>::setValue(const T &v){
        ValueChange<T> vc(this);
        vc.type = Change::Value;
        vc.before = *m_v;
        *m_v = v;
        vc.after = v;
        vc.sender = this;
        vc.origin = &vc;
        this->notify(vc);
    }

}
#endif // VALUE_H
