#include "line.h"

using namespace Model;

Line::Line(){// : NamedObject(IDObject::Line) {

}

void Line::addKeyPoint(KeyPoint *p){
    m_keypoints.push_back(p);
    connect(p, [this](ObjectChange&c) { onKeyPointChanged(c); });
}

void Line::onKeyPointChanged(ObjectChange &c) {
    switch (c.type){
        case Change::Deleted: {
            CorruptedChange cc(this);
            cc.origin = &c;
            notify(cc);
        } break;
        case Change::Object:{
            ObjectChange lc(this);
            lc.origin = &c;
            notify(lc);
        } break;        
        default:
        break;
    }
}

void Line::serialize(ostringstream &t, int indent) const {
    NamedObject::serialize(t, indent);
    t << ",";
    handleIndent(t, indent);
    t << "\"keypoints\": [";
    for (auto const &l : m_keypoints) {
        if (l != this->m_keypoints[0]) t << ",";
        handleIndent(t, indent==-1? indent : indent+2);
        t << (string)l->name;
    }
    handleIndent(t, indent);
    t << "]";
}

/*string Line::toString() const{
    ostringstream t;
    t << "{ t: " << m_type;
    t << ", name: " << (string)name;
    t << ", kps: [" ;
    for (auto l : m_keypoints) {
        if (l != m_keypoints.front()) t << ", ";
        t << l->toString();
    }
    t << " ]}";
    return t.str();
}*/
