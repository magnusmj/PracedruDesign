#ifndef DATAMAP_H
#define DATAMAP_H

#include "namedobject.h"

template <typename T>
class DataMap : map<ulong, T>, public NamedObject {

public:    
    DataMap() {  }
    T* Create(string name) {

    }
    T* Get(string name);
    T* Get(long id);
};

#endif // DATAMAP_H
