#ifndef VERTEX_H
#define VERTEX_H

namespace Model {
    template <typename V> class Vector3 {
    protected:
        V m_x = 0, m_y = 0, m_z = 0;
    };

    template <typename V> class Vector4 {
    protected:
        V m_x = 0, m_y = 0, m_z = 0, m_w = 0;
    };
}
#endif // VERTEX_H
