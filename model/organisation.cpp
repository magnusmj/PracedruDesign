#include "organisation.h"

using namespace Model;

Organisation::Organisation() : ParametersHolder { nullptr, Type::ORGANISATION} {

}

MapOfNamed<Project> *Organisation::getProjects(){
    return &m_projects;
}


void Organisation::serialize(ostringstream &t, int indent) const {
    NamedObject::serialize(t, indent);
    t << ",";
    handleIndent(t, indent);
    t << "\"projects\": " << m_projects.toString(indent);
}
