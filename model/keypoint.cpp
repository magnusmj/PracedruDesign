#include "keypoint.h"

using namespace Model;

KeyPoint::KeyPoint() : x{m_x}, y{m_y}, z{m_z} {//}, NamedObject(IDObject::KeyPoint) {
    connect(&x, [this](ObjectChange&c) { onChange(c, Indice::X); });
    connect(&y, [this](ObjectChange&c) { onChange(c, Indice::Y); });
    connect(&z, [this](ObjectChange&c) { onChange(c, Indice::Z); });
}

KeyPoint::operator const string() const {
    return toString();
}

void KeyPoint::serialize(ostringstream &t, int indent) const {
    NamedObject::serialize(t, indent);
    t << ",";
    handleIndent(t, indent);
    t << "\"x\": " << m_x << ",";
    handleIndent(t, indent);
    t << "\"y\": " << m_y << ",";
    handleIndent(t, indent);
    t << "\"z\": " << m_z;
}

void KeyPoint::onChange(ObjectChange &change, Indice i) {
    if (change.type == Change::Value) {
        KeyPointChange kpc(this);
        kpc.origin = &change;
        kpc.indice = i;
        notify(kpc);
    }
}

KeyPointChange::KeyPointChange(KeyPoint *sender) : ObjectChange { sender } {
    //qDebug() << "new KeyPointChang sebder: " << this->sender;
}
