#ifndef PARAMETERSHOLDER_H
#define PARAMETERSHOLDER_H

#include "parameter.h"
#include "mapofnamed.h"

namespace Model{
    class ParametersHolder : public NamedObject {
        MapOfNamed<Parameter> m_parameters;
        ParametersHolder * m_parent = nullptr;
    public:
        ParametersHolder(ParametersHolder *parent = nullptr, Type type=Type::NOTSET);
        Parameter* createParameter(string name);
        ParametersHolder *parent() const;
        void setParent(ParametersHolder *newParent);
    };
}
#endif // PARAMETERSHOLDER_H
