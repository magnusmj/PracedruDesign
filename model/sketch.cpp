#include "sketch.h"

using namespace Model;

Sketch::Sketch(){

}

MapOfNamed<KeyPoint> *Sketch::getKeyPoints() {
    return &m_keypoints;
}

KeyPoint *Sketch::createKeyPoint() {
    string name = "kp" + to_string(m_kpcounter++);
    return m_keypoints.create(name);
}

Line *Sketch::createLine() {
    string name = "line" + to_string(m_linecounter++);
    return m_lines.create(name);
}

void Sketch::serialize(ostringstream &t, int indent) const {
    NamedObject::serialize(t, indent);
    t << ",";
    /*handleIndent(t, indent);
    t << "\"type\": " << m_type << ",";*/
    handleIndent(t, indent);
    t << "\"keypoints\": " << m_keypoints.toString(indent);
    t << "\"lines\": " << m_lines.toString(indent);
}
