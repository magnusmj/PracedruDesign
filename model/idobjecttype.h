#ifndef IDOBJECTTYPE_H
#define IDOBJECTTYPE_H

namespace Model{
    struct IDObject {
        typedef enum {
            Base,
            Organization,
            Projects,
            Project,
            ParametersHolder,
            Parameters,
            Parameter,
            KeyPoints,
            KeyPoint,
            Lines,
            Line,
            Sketches,
            Sketch
        } Type;
    };
}
#endif // IDOBJECTTYPE_H
