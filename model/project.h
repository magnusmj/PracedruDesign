#ifndef PROJECT_H
#define PROJECT_H

#include "mapofnamed.h"

#include "parametersholder.h"
#include "sketch.h"

namespace Model {
    class Project : public ParametersHolder {
        MapOfNamed<Sketch> m_sketches;
    public:
        Project();
        MapOfNamed<Sketch> * getSketches();
        void serialize(ostringstream &t, int indent) const override;
    };
}
#endif // PROJECT_H
