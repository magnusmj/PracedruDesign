#include "namedobject.h"

using namespace Model;

NamedObject::NamedObject(Type type) : name(m_name), BaseObject(type) {
    connect(&name, [this](ObjectChange &c){ onNameChanged(c); });
}


void NamedObject::serialize(ostringstream &t, int indent) const {
    handleIndent(t, indent);
    t << "\"name\": \"" << (string)name << "\"";
}

void NamedObject::onNameChanged(ObjectChange &c) {
    if (c.type == Change::Value){        
        ValueChange<string> change = *(ValueChange<string>*)&c;
        change.sender = this;
        change.type = Change::Name;
        notify(change);
    }
}
