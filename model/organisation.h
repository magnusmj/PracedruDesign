#ifndef ORGANISATION_H
#define ORGANISATION_H

//#include "base.h"
#include "mapofnamed.h"
#include "project.h"

namespace Model {
    class Organisation : public ParametersHolder {
        //Root *m_root;
        MapOfNamed<Project> m_projects;
    public:
        Organisation();
        MapOfNamed<Project> * getProjects();
        void serialize (ostringstream &t, int indent) const override;
    };
}
#endif // ORGANISATION_H
