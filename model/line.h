#ifndef LINE_H
#define LINE_H

#include "keypoint.h"
#include "namedobject.h"

namespace Model{
    class Line : public NamedObject {
        vector<KeyPoint*> m_keypoints;
    public:
        typedef enum {
            Linear,
            Arc,
            Ellipse,
            Poly,
            Fillet,
            Circle,
            Spline,
            Nurbs
        } Type;
        Line();
        void addKeyPoint(KeyPoint * p);
        string toString() const;
    private:
        Type m_line_type = Type::Linear;
        void onKeyPointChanged(ObjectChange &c);
        void onLineChanged(ObjectChange &c);
        void serialize (ostringstream &t, int indent) const override;
    };
}
#endif // LINE_H
