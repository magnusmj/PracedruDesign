#ifndef MAPOFNAMED_H
#define MAPOFNAMED_H

#include "namedobject.h"
#include "observer.h"

#include <qdebug.h>
#include <string>

namespace Model{
    template <typename T>
    class MapOfNamed : public map<string , T>, public BaseObject {
        struct AddedChange : ObjectChange {
            T* added;
            AddedChange(IObservable* s, T* added) : ObjectChange(s) {
                type = Change::Added;
                this->added = added;
                origin = this;
            }
        };
    public:
        MapOfNamed (){}// (IDObject::Type type) : BaseObject { type } { }
        T *create(string name){
            T * p = &this->operator[](name);
            ((NamedObject*)p)->name = name;
            MapOfNamed<T>::AddedChange c(this, p);
            c.origin = &c;
            notify(c);
            connect(p, [this](ObjectChange &c) { onItemChanged(c); } );
            return p;
        }
        T *get(string name) {
            if (map<string , T>::find(name) != map<string , T>::end()) {
                return &map<string , T>::operator[](name);
            }
            return nullptr;
        }        
        int size(){ return MapOfNamed<T>::size(); }

        void serialize(ostringstream &t, int indent) const override {
            handleIndent(t, indent);
            for (auto const &tpl : *this) {
                if (&tpl.second != &this->begin()->second) {
                    t << ", ";
                    handleIndent(t, indent);
                }
                t << "\"" << tpl.first << "\": ";
                t << ((NamedObject*)&tpl.second)->toString(indent==-1?-1:indent);
            }
        }
    private:
        void onItemChanged(ObjectChange &c){
            if (c.type == Change::Name){
                qDebug() << "MapOfNamed::onItemChanged reanmed";

                ValueChange<string> *vc = (ValueChange<string> *)&c;
                qDebug() << ((NamedObject*)vc->sender)->toString().c_str();
                auto nodeHandler = this->extract(vc->before);
                nodeHandler.key() = vc->after;
                this->insert(std::move(nodeHandler));
            }
        }
    };
}

#endif // MAPOFNAMED_H
