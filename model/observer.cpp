#include "observer.h"

#include <iostream>

using namespace Model;

Observable::Observable() {

}

Observable::~Observable(){
    DeletedChange c(this);
    c.sender = this;
    notify(c);
}


void Observable::attach(IObserver *observer) {
    m_observers.push_back(observer);
}


void Observable::detach(IObserver *observer) {
    m_observers.remove(observer);
}


void Observable::notify(ObjectChange &change) {
    for (auto observer : m_observers) {
        observer->onChange(change);
    }
}

Observer::~Observer() {
    for (auto o : m_observables) o.first->detach(this);
}

void Observer::connect(IObservable *observable, function<void (ObjectChange&)> callback) {
    observable->attach(this);
    m_observables[observable] = callback;
    //m_callback = callback;
}

void Observer::onChange(ObjectChange &change) {
    m_observables[change.sender](change);
    if (change.type == Change::Deleted) {
        DeletedChange* dc = (DeletedChange*)(&change);
        m_observables.erase(dc->sender);
    }
}

ObjectChange::ObjectChange(IObservable *s) {
    this->sender = s;
    this->type = Change::Object;
}

DeletedChange::DeletedChange(IObservable *s) : ObjectChange(s) {
    type = Change::Deleted;
    origin = this;
}


CorruptedChange::CorruptedChange(IObservable *s) : ObjectChange(s) {
    type = Change::Corrupted;
    origin = this;
}
