#include "root.h"

using namespace Model;

Root::Root() : NamedObject(Type::ROOT) { }

Model::Organisation *Model::Root::createOrganisation(string name) {
    auto org = m_organisations.create(name);
    org->name = name;
    connect(org, [this](ObjectChange &c) { onOrganisationRenamed(c); } );
    return org;
}

void Root::onOrganisationRenamed(ObjectChange &c) {

}
