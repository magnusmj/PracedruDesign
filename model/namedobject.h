#ifndef NAMEDOBSERVEOBJECT_H
#define NAMEDOBSERVEOBJECT_H

#include "../shared/stringify.h"

#include "value.h"
#include <iostream>

namespace Model {
    class BaseObject : public Observable, public Observer, public Stringify {
    public:
        typedef enum {
            NOTSET,
            ROOT,
            ORGANISATION,
            PROJECT,
            SKETCH,
            KEYPOINT
        } Type;
        BaseObject(Type type=Type::NOTSET) : m_type(type) {}
    protected:
        Type m_type = Type::NOTSET;
    };

    class NamedObject : public BaseObject {
        string m_name;
    public:
        Value<string> name;
        NamedObject(Type type=Type::NOTSET);
        void serialize (ostringstream &t, int indent) const override;
    private:
        void onNameChanged(ObjectChange &c);
    };
}


#endif // NAMEDOBSERVEOBJECT_H
