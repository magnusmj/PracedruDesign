#ifndef OBSERVER_H
#define OBSERVER_H

#include <list>
#include <map>
#include <functional>

using namespace std;

namespace Model {
    struct Change {
        typedef enum {
            Object,
            Value,
            Name,
            Added,
            Removed,
            Deleted,
            Corrupted
        } Type;
    };

    class IObservable;

    struct ObjectChange{
        Change::Type type;
        ObjectChange *origin = nullptr;
        IObservable *sender = nullptr;
        ObjectChange(IObservable *sender);
    };

    class IObserver {
    public:
        virtual ~IObserver(){};
        virtual void onChange(ObjectChange &change) = 0;
    };

    class IObservable {
    public:
        virtual ~IObservable(){};
        virtual void attach(IObserver *observer) = 0;
        virtual void detach(IObserver *observer) = 0;
    };



    struct DeletedChange : public ObjectChange {
        DeletedChange(IObservable* s);
    };

    struct CorruptedChange: public ObjectChange {
        CorruptedChange(IObservable* s);
    };

    class Observer : public IObserver {
        map<IObservable *, function<void(ObjectChange&)>> m_observables;
        //function<void(IChange&)> m_callback;
    public:
        Observer() { }
        ~Observer();
        void connect(IObservable *observable, function<void(ObjectChange&)> callback);
        void onChange(ObjectChange &change) override;
    };

    class Observable : public IObservable {
        list<IObserver *> m_observers;
    public:
        Observable();
        ~Observable();
        void attach(IObserver *observer) override;
        void detach(IObserver *observer) override;
        void notify(ObjectChange &change);
    };
}

#endif // OBSERVER_H

