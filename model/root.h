#ifndef MODELBASE_H
#define MODELBASE_H

#include "namedobject.h"
#include "organisation.h"

namespace Model {
    class Root : public NamedObject {
        MapOfNamed<Organisation> m_organisations;

    public:
        Root();
        Organisation *createOrganisation(string name);
    private:
        void onOrganisationRenamed(ObjectChange &c);
    };
}
#endif // MODELBASE_H
