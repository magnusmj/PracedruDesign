#include "project.h"

using namespace Model;

Project::Project() /*: NamedObject(IDObject::Project),
    m_sketches(IDObject::Sketches)*/{

}

MapOfNamed<Sketch> *Project::getSketches() {
    return &m_sketches;
}

void Project::serialize(ostringstream &t, int indent) const {
    NamedObject::serialize(t, indent);
    t << ",";
    handleIndent(t, indent);
    t << "\"sketches\": " << m_sketches.toString(indent);
}

