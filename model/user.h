#ifndef USER_H
#define USER_H

#include "namedobject.h"

#include <string>
#include <vector>


using namespace std;

namespace Model{
    class User : public NamedObject {
        int m_id;        
        vector<string> m_tokens;
    public:
        User();
        bool checkToken(string token);
        string createToken();
        void setID(int id) { m_id = id; }
    };
}
#endif // USER_H
