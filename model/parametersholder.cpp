#include "parametersholder.h"

using namespace Model;

ParametersHolder *ParametersHolder::parent() const {
    return m_parent;
}

void ParametersHolder::setParent(ParametersHolder *newParent) {
    m_parent = newParent;
}

ParametersHolder::ParametersHolder(ParametersHolder *parent, Type type)
    : NamedObject(type) {
    m_parent = parent;
}

Parameter *ParametersHolder::createParameter(string name) {
    Parameter * p = m_parameters.create(name);
    p->setParent(this);
    return p;
}
