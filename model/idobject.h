#ifndef IDOBJECT_H
#define IDOBJECT_H

#include "value.h"
#include "idobjecttype.h"

#include <map>

using namespace std;

namespace Model {
    class IdObject {
        static inline unsigned long counter;
        static inline map<unsigned long, IdObject*> id_objects;
        unsigned long m_id;
        IdObject(unsigned long id, IDObject::Type type);
    public:
        const IDObject::Type m_type;
        const Value<unsigned long> id;

        IdObject(IDObject::Type type);
        ~IdObject() { id_objects.erase(m_id); }
        static IdObject *getIdObject(unsigned long id);
    };
}
#endif // IDOBJECT_H
