#ifndef PARAMETER_H
#define PARAMETER_H

#include "value.h"
#include "observer.h"
#include "namedobject.h"
#include "exprtk.hpp"

#include <iostream>

typedef exprtk::symbol_table<double> SymbolTable;
typedef exprtk::expression<double>   Expression;
typedef exprtk::parser<double>       Parser;

using namespace std;

namespace Model {
    class ParametersHolder;

    class Parameter : public NamedObject {
        string m_formula;
        double m_value;
        bool locked;
        SymbolTable m_symbol_table;
        Expression m_expression;
        Parser m_parser;
        ParametersHolder *m_parent;
    public:
        Value<double> value;
        Value<string> formula;
        Parameter();
        void setParent(ParametersHolder *parent);
    private:
        void onValueChange(ObjectChange& change);
        void onFormulaChange(ObjectChange& change);
    };

    class ParameterChange : ObjectChange {

    };
}
#endif // PARAMETER_H
