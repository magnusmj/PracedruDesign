#ifndef SKETCH_H
#define SKETCH_H

#include "namedobject.h"
#include "keypoint.h"
#include "mapofnamed.h"
#include "line.h"
#include "parametersholder.h"

namespace Model {
    class Sketch : public ParametersHolder {
        int m_kpcounter = 0;
        int m_linecounter = 0;
        MapOfNamed<KeyPoint> m_keypoints;
        MapOfNamed<Line> m_lines;
    public:
        Sketch();
        MapOfNamed<KeyPoint> * getKeyPoints();
        MapOfNamed<Line> * getLines();
        KeyPoint *createKeyPoint();
        Line *createLine();
        void serialize (ostringstream &t, int indent) const override;
    };
}
#endif // SKETCH_H
