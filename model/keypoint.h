#ifndef KEYPOINT_H
#define KEYPOINT_H

#include "namedobject.h"
#include "observer.h"
#include "value.h"
#include "vertex.h"

#include <list>
#include <iostream>
#include <sstream>

using namespace std;

namespace Model{
    class KeyPoint : public NamedObject, Vector3<double> {
    public:
        typedef enum {
            X,
            Y,
            Z
        } Indice;
        Value<double> x,y,z;
        KeyPoint ();
        operator const string() const;
        //string toString() const;
        void serialize (ostringstream &t, int indent) const override;
    private:
        void onChange(ObjectChange &change, Indice i);
    };

    struct KeyPointChange: ObjectChange{
        KeyPoint::Indice indice;
        KeyPointChange(KeyPoint *sender);
    };
}
#endif // KEYPOINT_H
