#include "parameter.h"

#include <qdebug.h>

using namespace Model;

Parameter::Parameter()
    : value{m_value}, formula(m_formula) {//}, NamedObject(IDObject::Parameter) {
    connect(&value, [this](ObjectChange& ch){ onValueChange(ch); });
    connect(&formula, [this](ObjectChange& ch){ onFormulaChange(ch); });
}

void Parameter::setParent(ParametersHolder *parent) {
    m_parent = parent;
}

void Parameter::onValueChange(ObjectChange& change) {
    qDebug() << "Parameter value changed";
}

void Parameter::onFormulaChange(ObjectChange &change) {
    m_symbol_table.clear();
    double x;
    m_symbol_table.add_variable("x",x);
    m_symbol_table.add_constants();
    //m_expression;
    m_expression.register_symbol_table(m_symbol_table);
    //Parser parser;

    m_parser.compile(m_formula, m_expression);
    m_value = m_expression.value();
}


