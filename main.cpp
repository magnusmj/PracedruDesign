#include "controller/controller.h"
#include "view/mainwindow.h"

#include <QApplication>

using namespace Controller;

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);    
    Control *c = Control::getInstance();
    MainWindow w(c);
    w.show();
    return a.exec();
}
