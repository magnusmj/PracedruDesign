#ifndef TREEVIEWDOCK_H
#define TREEVIEWDOCK_H

#include <QFileSystemModel>
#include <QDockWidget>
#include <QTreeView>

class MainWindow;

class TreeViewDock : public QDockWidget
{
    Q_OBJECT
    MainWindow *m_main_window;
    QTreeView m_tree_view;
    QItemSelectionModel *m_selection_model;
    QFileSystemModel *m_model;
public:
    explicit TreeViewDock(MainWindow *main_window = nullptr);
private:
    bool eventFilter(QObject* object, QEvent* event);
    void onDelete();
signals:
private slots:
    void onTreeSelectionChanged(QItemSelection,QItemSelection);
    void onNewItemAdded(QModelIndex item_index);
};

#endif // TREEVIEWDOCK_H
