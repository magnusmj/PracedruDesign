#include "treeviewdock.h"
#include "mainwindow.h"

#include <QFileSystemModel>
#include <QTreeView>
#include <QEvent>
#include <QKeyEvent>

TreeViewDock::TreeViewDock(MainWindow *main_window)
    : QDockWidget{main_window} {
    m_main_window = main_window;
    setWindowTitle(tr("Project view"));
    setObjectName("TreeViewDock");
    //_doc = document

    setWidget(&m_tree_view);

    /* temporary model for debugging */
    QString mPath = "/home/magnus";
    m_model = new QFileSystemModel(this);
    m_model->setFilter(QDir::NoDotAndDotDot | QDir::AllEntries);
    m_model->setRootPath(mPath);
    m_tree_view.setModel(m_model);
    /* temporary model for debugging */

    //m_model = DocumentItemModel(document);
    //m_tree_view.setModel(_model);
    //m_tree_view.setRootIndex(m_model.index(0, 0));

    m_selection_model = m_tree_view.selectionModel();
    connect(m_selection_model, SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
        this, SLOT(onTreeSelectionChanged(QItemSelection,QItemSelection)));

    //connect(m_model, SIGNAL(newItemAdded(QModelIndex), this, SLOT(onNewItemAdded(QModelIndex)));
    installEventFilter(this);
}

bool TreeViewDock::eventFilter(QObject *object, QEvent *event) {
    if (event->type() == QEvent::KeyPress){
        QKeyEvent *e = (QKeyEvent *)event;
        if (e->key() == Qt::Key_Delete) {
            onDelete();
            return true;
        }
    }
    return false;
}

void TreeViewDock::onDelete() {
    qDebug() << "TreeViewDock::onDelete()";
}

void TreeViewDock::onTreeSelectionChanged(QItemSelection, QItemSelection) {
    qDebug() << "TreeViewDock::onTreeSelectionChanged";
    auto indexes = m_selection_model->selectedIndexes();
}

void TreeViewDock::onNewItemAdded(QModelIndex item_index){

}
