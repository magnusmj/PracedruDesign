#include "mainwindow.h"
#include "qdir.h"
#include "treeviewdock.h"
#include "quribbon/quribbonbutton.h"
#include "quribbon/quribbonmisc.h"

#include <QMessageBox>
#include <QToolBar>
#include <QStandardPaths>

#include "../model/organisation.h"

void test(){
    Model::Organisation org;
    org.name = "Flimflam corp 200";
    Model::Observer o;    
    auto projects = org.getProjects();
    o.connect(projects, [](Model::ObjectChange&c){
        qDebug() << "org changed: " << c.type;
    });
    auto p = projects->create("Test Project");
    auto s = p->getSketches()->create("New Sketch");
    auto kp0 = s->createKeyPoint();
    auto kp1 = s->createKeyPoint();
    auto line = s->createLine();
    p->name = "Project Ajax 2";
    line->addKeyPoint(kp0);
    line->addKeyPoint(kp1);       
    kp0->y = 20;
    kp1->x = 30;
    qDebug() << s->getKeyPoints()->toString(0).c_str();
}

MainWindow::MainWindow(Control *controller, QWidget *parent) : QMainWindow(parent) {
    m_controller = controller;
    setMinimumHeight(800);
    setMinimumWidth(1280);
    setWindowIcon(*getIcon("icon"));
    m_ribbon = new QuRibbon(this);
    connect(m_ribbon, SIGNAL(changed(int)), this, SLOT(onRibbonChanged(int)));
    initActions();
    initRibbon();
    initViews();
}

MainWindow::~MainWindow() {

}

void MainWindow::initConfig(){
    auto p = QStandardPaths::standardLocations(QStandardPaths::AppLocalDataLocation)[0];
    QDir path(p);
    if (!path.exists()) {
        //path.mkpath(p);
        path.mkpath(p+"/model");
    } else {

    }

    qDebug() << p;
}

QAction *MainWindow::createAction(QString caption, QString icon_name, QString status_tip, bool icon_visible, QKeySequence shortcut, bool checkable){
    auto icon = getIcon(icon_name);
    QAction *action;
    if (icon)
        action = new QAction(*icon, tr(caption.toStdString().c_str(), "ribbon"), this);
    else
        action = new QAction(tr(caption.toStdString().c_str(), "ribbon"), this);
    action->setStatusTip(tr(status_tip.toStdString().c_str(), "ribbon"));
    action->setIconVisibleInMenu(icon_visible);
    if (shortcut == 0)
        action->setShortcut(shortcut);
    action->setCheckable(checkable);
    addAction(action);
    return action;
}

void MainWindow::initActions() {
    m_new_action = createAction("New\nFile", "newicon", "New Document", true, QKeySequence::New);
    connect(m_new_action, SIGNAL(triggered()), this, SLOT(onNewDocument()));
    m_open_action = createAction("Open\nFile", "open", "Open file", true, QKeySequence::Open);
    m_save_action = createAction("Save", "save", "Save these data", true, QKeySequence::Save);
    m_save_as_action = createAction("Save\nas", "saveas", "Save these data as ...", true, QKeySequence::SaveAs);
    m_undo_action = createAction("Undo", "undo", "Undo last action", true, QKeySequence::Undo);
    m_redo_action = createAction("Redo", "redo", "Redo last undone action", true, QKeySequence::Redo);
    m_undo_action->setEnabled(false);
    m_redo_action->setEnabled(false);

    m_add_sketch_to_document_action = createAction("Add\nSketch", "addsketch", "Add Sketch to the document", true);
    m_add_drawing_action = createAction("Add\nDrawing", "adddrawing", "Add drawing to the document", true);
    m_add_part_action = createAction("Add\nPart", "addpart", "Add part to the document", true);
    m_add_calc_table_analysis = createAction("Add Calc\nTable", "addcalctable", "Add calculation table analysis to document", true);
    m_add_calc_sheet_analysis = createAction("Add Calc\nSheet", "addcalcsheet", "Add calculation sheet analysis to document", true);

    m_about_action = createAction("About", "about", "About this appliction", true);
    connect(m_about_action, SIGNAL(triggered()), this, SLOT(onAbout()));
    m_credits_action = createAction("Credit", "credit", "Credit to open source software used with PracedruDesign", true);
}

void MainWindow::initRibbon() {
    initHomeTab();
    initHelpTab();
}

void MainWindow::initHomeTab() {
    auto home_tab = m_ribbon->createRibbonTab(tr("Home"));
    auto file_pane = home_tab->createRibbonPane(tr("File"));
    file_pane->addRibbonWidget(new QuRibbonButton(file_pane, m_new_action, true));
    file_pane->addRibbonWidget(new QuRibbonButton(file_pane, m_open_action, true));
    file_pane->addRibbonWidget(new QuRibbonButton(file_pane, m_save_action, true));
    file_pane->addRibbonWidget(new QuRibbonButton(file_pane, m_save_as_action, true));
    auto edit_pane = home_tab->createRibbonPane(tr("Edit"));
    edit_pane->addRibbonWidget(new QuRibbonButton(edit_pane, m_undo_action, true));
    edit_pane->addRibbonWidget(new QuRibbonButton(edit_pane, m_redo_action, true));
    auto insert_pane = home_tab->createRibbonPane(tr("Insert"));
    insert_pane->addRibbonWidget(new QuRibbonButton(insert_pane, m_add_sketch_to_document_action, true));
    insert_pane->addRibbonWidget(new QuRibbonButton(insert_pane, m_add_part_action, true));
    insert_pane->addRibbonWidget(new QuRibbonButton(insert_pane, m_add_drawing_action, true));
    insert_pane->addRibbonWidget(new QuRibbonButton(insert_pane, m_add_calc_table_analysis, true));
    insert_pane->addRibbonWidget(new QuRibbonButton(insert_pane, m_add_calc_sheet_analysis, true));
}

void MainWindow::initHelpTab() {
    auto help_tab = m_ribbon->createRibbonTab(tr("Help"));
    auto info_pane = help_tab->createRibbonPane(tr("Info"));
    info_pane->addRibbonWidget(new QuRibbonButton(info_pane, m_about_action, true));
    info_pane->addRibbonWidget(new QuRibbonButton(info_pane, m_credits_action, true));
}

void MainWindow::initViews() {
    m_tree_view_dock = new TreeViewDock(this);
    addDockWidget(Qt::LeftDockWidgetArea, m_tree_view_dock);
    m_view_widget = new CentralViewWidget(this);
    setCentralWidget(m_view_widget);
}

void MainWindow::onRibbonChanged(int index) {
    //qDebug() << "ribbon changed: " << index;
}

void MainWindow::onNewDocument() {
    qDebug() << "onNewDocument";
    test();
}

void MainWindow::onAbout() {
    QString msg = "Design and Analysis tool for makers.\n"
        "Programmed and Copyright © by Magnus Jørgensen.\n"
        "This program is licensed with MIT license.";
    QMessageBox::about(this, "About Pracedru Design", msg);

}

