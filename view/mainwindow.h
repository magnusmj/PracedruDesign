#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "quribbon/quribbon.h"
#include "../controller/controller.h"
#include "centralviewwidget.h"
#include "treeviewdock.h"

#include <QMainWindow>

using namespace Controller;

class MainWindow : public QMainWindow {
    Q_OBJECT
    QuRibbon *m_ribbon;
    QAction *m_new_action;
    QAction *m_open_action;
    QAction *m_save_action;
    QAction *m_save_as_action;
    QAction *m_undo_action;
    QAction *m_redo_action;
    QAction *m_add_sketch_to_document_action;
    QAction *m_add_part_action;
    QAction *m_add_drawing_action;
    QAction *m_add_calc_table_analysis;
    QAction *m_add_calc_sheet_analysis;
    QAction *m_about_action;
    QAction *m_credits_action;
    Control *m_controller;
    TreeViewDock *m_tree_view_dock;
    CentralViewWidget *m_view_widget;
public:
    MainWindow(Control *controller, QWidget *parent = nullptr);
    ~MainWindow();
private:
    void initConfig();
    void initActions();
    void initRibbon();
    void initHomeTab();
    void initHelpTab();
    void initViews();
    QAction *createAction(QString caption, QString icon_name, QString status_tip, bool icon_visible, QKeySequence shortcut = 0, bool checkable=false);
private slots:
    void onRibbonChanged(int index);
    void onNewDocument();
    void onAbout();
};
#endif // MAINWINDOW_H
