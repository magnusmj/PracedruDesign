#include "stringify.h"


string Stringify::toString(int indent) const {
    ostringstream t;
    t << "{";
    serialize(t, indent == -1 ? -1 : indent+2);
    handleIndent(t, indent);
    t << "}";
    return t.str();
}

void Stringify::handleIndent(ostringstream &t, int indent) const{
    if (indent < 0) t << " ";
    else {
        t << "\n";
        for (int i=0; i<indent; i++) t << " ";
    }
}
