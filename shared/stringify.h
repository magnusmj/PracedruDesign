#ifndef STRINGIFY_H
#define STRINGIFY_H

#include <string>
#include <sstream>

using namespace std;

class Stringify {
public:
    Stringify(){}
    virtual void serialize (ostringstream &t, int indent) const = 0;
    string toString(int indent=-1) const;
protected:
    void handleIndent(ostringstream &t, int indent) const;
};


#endif // STRINGIFY_H
