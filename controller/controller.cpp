#include "controller.h"

using namespace Controller;

Control::Control(int port, QObject *parent)
    : QObject(parent), m_server(this) {
    connect(&m_server, SIGNAL(actionRecieved(Action&)), this, SLOT(onAction(Action&)));
    if (!m_server.start(port)) {
        m_client.connect(QUrl("ws://localhost:7789"));
    }
}

Control *Control::getInstance() {
    if (instance==nullptr) instance = new Control(PORT);
    return Control::instance;
}

void Control::onAction(Action &action) {
    if (m_is_remote){
        m_client.sendAction(action);
    }
}
