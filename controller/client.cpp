#include "client.h"
#include "message.h"

#include <QJsonDocument>
#include <QJsonParseError>

using namespace Controller;

Client::Client() {
    QObject::connect(this, SIGNAL(connected()),
                     this, SLOT(onConnected()));
    QObject::connect(this, SIGNAL(disconnected()),
                     this, SLOT(onClosed()));
}

void Client::connect(const QUrl &url) {
    qDebug() << url.port();
    open(url);
}

void Client::sendAction(Action &action){
    QString msg = QString(action.toString().c_str());
    sendTextMessage(msg);
}

void Client::handleJSON(QJsonObject &json) {
    Message::Type type = (Message::Type)json["type"].toInt(-1);
    switch(type) {
        case Message::Type::Handshake: {
            HandshakeMessage m(json);
            handleHandshake(m);
        } break;
        case Message::Type::Initialization:{
            InitializationMessage m(json);
            handleInitialization(m);
        } break;
        case Message::Type::Action: {
            ActionMessage m(json);
            handleAction(m);
        } break;
        default:{
            qDebug() << "Message type not valid: " << type;
            qDebug() << json;
        } break;
    }
}

void Client::handleHandshake(HandshakeMessage &m) {
    qDebug() << "handleHandshake";
    AuthenticationMessage am(m.user, "Bente", "Password");
    sendTextMessage(am.toString().c_str());
}

void Client::handleInitialization(InitializationMessage &m) {
    qDebug() << "handleInitialization";

}

void Client::handleAction(ActionMessage &m)
{

}

void Client::onConnected() {
    qDebug() << "client connected";
    QObject::connect(this, SIGNAL(textMessageReceived(QString)),
                this, SLOT(onTextMessageReceived(QString)));
    HandshakeMessage m(0);
    sendTextMessage(m.toString().c_str());
}

void Client::onClosed() {
    qDebug() << "client connection closed";
}

void Client::onTextMessageReceived(QString message) {
    QJsonParseError e;
    QJsonDocument doc= QJsonDocument::fromJson(message.toUtf8(), &e);
    if (e.error == QJsonParseError::NoError){
        auto json = doc.object();
        handleJSON(json);
    } else {
        qDebug() << "Errors";
        qDebug() << e.errorString();
        qDebug() << message;
    }
}



