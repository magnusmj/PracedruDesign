#include "message.h"

using namespace Controller;

void Message::serialize(ostringstream &t, int indent) const {
    handleIndent(t, indent);
    t << "\"type\": " << type;
}

ActionMessage::ActionMessage(QJsonObject &json) {

}

void ActionMessage::serialize(ostringstream &t, int indent) const {

}

AuthenticationMessage::AuthenticationMessage(QJsonObject &json){
    type = Type::Authentication;
    user = json["user"].toInt(-1);
    token = json["token"].toString().toStdString();
    password = json["password"].toString().toStdString();
}

AuthenticationMessage::AuthenticationMessage(int user, string token, string password) {
    type = Type::Authentication;
    this->user = user;
    this->token = token;
    this->password = password;
}

void AuthenticationMessage::serialize(ostringstream &t, int indent) const {
    Message::serialize(t, indent);
    t << ",";
    handleIndent(t, indent);
    t << "\"user\": " << user << ",";
    handleIndent(t, indent);
    t << "\"password\": \"" << password << "\",";
    handleIndent(t, indent);
    t << "\"token\": \"" << token << "\"";
}

HandshakeMessage::HandshakeMessage(QJsonObject &json){
    type = Type::Handshake;
    version = json["version"].toInt(-1);
}

HandshakeMessage::HandshakeMessage(int version) {
    type = Type::Handshake;
    this->version = version;
}

void HandshakeMessage::serialize(ostringstream &t, int indent) const {
    Message::serialize(t, indent);
    t << ",";
    handleIndent(t, indent);
    t << "\"version\": " << version;

}

InitializationMessage::InitializationMessage(QJsonObject &json) {
    type = Type::Initialization;
}

void InitializationMessage::serialize(ostringstream &t, int indent) const {

}
