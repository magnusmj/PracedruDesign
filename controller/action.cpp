#include "action.h"

using namespace Controller;

NamedAction::NamedAction(Type t, string n, int u) {
    this->type = t;
    this->name = n;
    this->user = u;
}

ChangeNameAction::ChangeNameAction(string n, int u, unsigned long i)
    : NamedAction(ChangeName, n, u) {
    this->id = i;
}
