#include "server.h"
#include "message.h"
#include "controller.h"

#include <QWebSocket>
#include <QJsonDocument>
#include <QJsonObject>
#include <QApplication>

using namespace Controller;

Server::Server(QObject *parent)
    : QWebSocketServer("Controller Server", QWebSocketServer::NonSecureMode, parent) {            
    connect(this, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
    connect(this, SIGNAL(closed()), this, SLOT(onClosed()));
}

bool Server::start(int port) {
    if (listen(QHostAddress::Any, port)){
        qDebug() << "listening on port: " << port;
        return true;
    }else{
        qDebug() << "port blocked: " << port;
        qDebug() << " running as client";
        return false;
    }
}

void Server::handleJSON(QJsonObject &json) {
    Message::Type type = (Message::Type)json["type"].toInt(-1);
    switch(type) {
        case Message::Type::Handshake: {
            HandshakeMessage m(json);
            handleHandshake(m);
        } break;
        case Message::Type::Authentication:{
            AuthenticationMessage m(json);
            handleAuthentication(m);
        } break;
        case Message::Type::Action: {
            ActionMessage m(json);
            handleAction(m);
        } break;
        default:{
            qDebug() << "Message type not valid: " << type;
            qDebug() << json;
        } break;
    }
}

void Server::handleHandshake(HandshakeMessage &m) {
    qDebug() << "handleHandshake";
    if (m.version == Control::getInstance()->getVersion()){
        qDebug() << "handhake accepted";
        QWebSocket *client = qobject_cast<QWebSocket *>(sender());
        HandshakeMessage m(0);
        client->sendTextMessage(m.toString().c_str());
    }
}

void Server::handleAuthentication(AuthenticationMessage &m) {
    qDebug() << "handleAuthentication";
    QWebSocket *client = qobject_cast<QWebSocket *>(sender());
    User *user = &m_user_clients[client];
    user->setID(m.user);
    qDebug() << "authenticated!";
    m_lobby.removeAll(client);
    emit userConnected(user);
}

void Server::handleAction(ActionMessage &m){
    qDebug() << "handleAction";
    QWebSocket *client = qobject_cast<QWebSocket *>(sender());

}

void Server::onNewConnection() {
    QWebSocket *client = nextPendingConnection();
    qDebug() << "client connected:" << client;
    connect(client, SIGNAL(textMessageReceived(QString)), this, SLOT(onTextMessage(QString)));
    connect(client, SIGNAL(binaryMessageReceived(QByteArray)), this, SLOT(onBinaryMessage(QByteArray)));
    connect(client, SIGNAL(disconnected()), this, SLOT(onSocketDisconnected()));
    m_lobby << client;

}

void Server::onClosed() {
    qDebug() << "websocket closed";
}

void Server::onSocketDisconnected() {
    QWebSocket *client = qobject_cast<QWebSocket *>(sender());
    qDebug() << "client disconnected:" << client;
    if (client) {
        m_user_clients.erase(client);
        m_lobby.removeAll(client);
        client->deleteLater();
    }
}

void Server::onTextMessage(QString message) {
    QWebSocket *client = qobject_cast<QWebSocket *>(sender());
    QJsonParseError e;
    QJsonDocument doc= QJsonDocument::fromJson(message.toUtf8(), &e);
    if (e.error == QJsonParseError::NoError){
        auto json = doc.object();
        handleJSON(json);
    } else {
        qDebug() << "Errors";
        qDebug() << e.errorString();
        qDebug() << message;
    }
    /*
    if (client) {
        client->sendTextMessage(message);
    }*/
}

void Server::onBinaryMessage(QByteArray message) {
    QWebSocket *client = qobject_cast<QWebSocket *>(sender());
    qDebug() << "Binary Message received:" << message;
    if (client) {
        client->sendBinaryMessage(message);
    }
}
