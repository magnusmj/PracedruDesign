#ifndef CONTROLLERMESSAGE_H
#define CONTROLLERMESSAGE_H

#include "action.h"
#include <QJsonObject>
#include <string>

using namespace std;

namespace Controller {    
    struct Message : Stringify {
        typedef enum {
            Handshake,
            Authentication,
            Initialization,
            Action
        } Type;
        Type type;    
        void serialize(ostringstream &t, int indent)const override;
    };
    struct HandshakeMessage : Message {
        int version;
        int user;
        HandshakeMessage(QJsonObject &json);
        HandshakeMessage(int version);
        void serialize(ostringstream &t, int indent)const override;
    };
    struct AuthenticationMessage : Message {
        int user;
        string token;
        string password;
        AuthenticationMessage(QJsonObject &json);
        AuthenticationMessage(int user, string token, string password);
        void serialize(ostringstream &t, int indent)const override;
    };
    struct ActionMessage : Message {
        unsigned long id;
        Controller::Action *action;
        ActionMessage(QJsonObject &json);
        void serialize(ostringstream &t, int indent)const override;
    };
    struct InitializationMessage: Message {
        InitializationMessage(QJsonObject &json);
        void serialize(ostringstream &t, int indent)const override;
    };
}



#endif // CONTROLLERMESSAGE_H
