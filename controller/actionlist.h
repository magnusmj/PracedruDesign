#ifndef ACTIONLIST_H
#define ACTIONLIST_H

#include "action.h"

namespace Controller {
    class ActionList {
        vector<Action> m_actions;
    public:
        explicit ActionList();
        void addAction(Action &action);
        Action &operator[](int i) { return m_actions[i]; }

    };
}

#endif // ACTIONLIST_H
