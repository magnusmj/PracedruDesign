#ifndef CONTROLLERCLIENT_H
#define CONTROLLERCLIENT_H

#include "action.h"
#include "message.h"

#include <QObject>
#include <QWebSocket>

namespace Controller {
    class Client : public QWebSocket {
        Q_OBJECT
    public:
        explicit Client();
        void connect(const QUrl &url);
        void sendAction(Action &action);
    private:
        void handleJSON(QJsonObject &json);
        void handleHandshake(HandshakeMessage &m);
        void handleInitialization(InitializationMessage &m);
        void handleAction(ActionMessage &m);
    signals:

    private slots:
        void onConnected();
        void onClosed();
        void onTextMessageReceived(QString message);
    };
}
#endif // CONTROLLERCLIENT_H
