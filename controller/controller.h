#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "action.h"
#include "client.h"
#include "server.h"

#include <QWebSocketServer>

const int CONTROLLERVERSION = 0;
const int PORT = 7789;

namespace Controller {
    class Control: public QObject {
        Q_OBJECT
        static inline Control *instance = nullptr;
        bool m_is_remote = false;
        Server m_server;
        Client m_client;
        Control(int port, QObject *parent = nullptr);
    public:        
        static Control *getInstance();
        void handleAction(Action &change);
        int getVersion() { return CONTROLLERVERSION; }
    private:

    private slots:
        void onAction(Action &);
    };
}
#endif // CONTROLLER_H
