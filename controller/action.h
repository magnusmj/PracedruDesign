#ifndef ACTION_H
#define ACTION_H

#include "../model/keypoint.h"

#include <iostream>

using namespace std;

namespace Controller {
    struct Action : public Stringify {
        typedef enum {
            CreateOrganization,
            CreateProject,
            CreatePrameter,
            CreateSketch,
            CreateKeypoint,
            CreateLine,
            ChangeName,
            ChangeValue
        } Type;
        Type type;
        int user;
        virtual ~Action() {}
    };

    struct NamedAction : Action {
        string name;
        NamedAction (Type type, string name, int user);
        ~NamedAction(){}
    };

    struct ChangeNameAction : NamedAction {
        unsigned long id;
        ChangeNameAction (string new_name, int user, unsigned long id);
        ~ChangeNameAction() {}
    };

    struct ChangeKeyPointAction {
        unsigned long id;
        Model::KeyPoint kp;
        ChangeKeyPointAction (int user, unsigned long id);
        ~ChangeKeyPointAction(){}
    };
}
#endif // ACTION_H
