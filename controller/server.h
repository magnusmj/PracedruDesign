#ifndef CONTROLLERSERVER_H
#define CONTROLLERSERVER_H

#include "action.h"
#include "message.h"
#include "../model/user.h"

#include <QObject>
#include <QWebSocketServer>

using namespace Model;

namespace Controller {
    class Server : public QWebSocketServer {
        Q_OBJECT
        map<QWebSocket*, User> m_user_clients;
        QList<QWebSocket*> m_lobby;
    public:
        explicit Server(QObject *parent = nullptr);
        bool start(int port);
    private:
        void handleJSON(QJsonObject &json);
        void handleHandshake(HandshakeMessage &m);
        void handleAuthentication(AuthenticationMessage &m);
        void handleAction(ActionMessage &m);
    private slots:
        void onNewConnection();
        void onClosed();
        void onSocketDisconnected();
        void onTextMessage(QString);
        void onBinaryMessage(QByteArray);

    signals:
        void userConnected(User *user);
        void actionRecieved(Action &action);
    };
}
#endif // CONTROLLERSERVER_H
